﻿using Newtonsoft.Json;

namespace Bit4YouClient.Lib.Models
{
    public class OAuthResult
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        [JsonProperty("token_type")]
        public string TokenType { get; set; }
        [JsonProperty("id_token")]
        public string IdToken { get; set; }
        [JsonProperty("expires_in")]
        public int ExpiresInSeconds { get; set; }
        [JsonProperty("auth_exp")]
        public int AuthExpInSeconds { get; set; }
    }
}
