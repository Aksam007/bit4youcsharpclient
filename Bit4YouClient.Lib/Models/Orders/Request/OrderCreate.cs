﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Bit4YouClient.Lib.Models.Orders.Request
{
    public class OrderCreate : Simulations
    {
        public string Txid { get; set; }
        public string ClientId { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public TimingForce TimingForce { get; set; }
        public string Market { get; set; }
        [System.Text.Json.Serialization.JsonConverter(typeof(StringEnumConverter))]
        public string Type { get; set; }
        public int Quantity { get; set; }
        [JsonProperty("quantity_iso")]
        public string QuantityIso { get; set; }
        public double Rate { get; set; }
    }

}
