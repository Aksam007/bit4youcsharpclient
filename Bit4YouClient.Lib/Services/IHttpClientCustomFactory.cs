﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bit4YouClient.Lib.Models;

namespace Bit4YouClient.Lib.Services
{
   public interface IHttpClientCustomFactory
   {
       Task<T> PerformNetworkCall<T>(ApiAction apiAction, string resourceUri=default,
           object requestObject = default, Dictionary<string, string> queryParams = default);
   }
}
