﻿using System.Collections.Generic;

namespace Bit4YouClient.Lib.Models.Market.Response
{
    public class MarketOrderBookResponse
    {
        public List<Ask> Ask { get; set; }
        public List<Bid> Bid { get; set; }
    }

    public class Ask
    {
        public string Quantity { get; set; }
        public string Rate { get; set; }
        public int I { get; set; }
    }

    public class Bid
    {
        public string Quantity { get; set; }
        public string Rate { get; set; }
        public int I { get; set; }
    }
}
