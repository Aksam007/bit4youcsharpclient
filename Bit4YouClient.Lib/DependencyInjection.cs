﻿using Bit4YouClient.Lib.Services;
using Bit4YouClient.Lib.Services.Impl;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Bit4YouClient.Lib
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddBit4YouClient(this IServiceCollection services, IConfiguration configuration)
        {
             services.AddMemoryCache();
             services.Configure<ClientSettings>(configuration.GetSection(nameof(ClientSettings)));
            services.AddTransient<IOAuthClient, OAuthClient>();
            services.AddTransient<IHttpClientCustomFactory, HttpClientCustomFactory>();
            services.AddTransient<ICacheService, CacheService>();
            services.AddTransient<IBit4YouClient, Services.Impl.Bit4YouClient>();
            services.AddTransient<IHttpClientCustomFactory, HttpClientCustomFactory>();
            services.AddHttpClient();
            return services;
        }
    }
}
