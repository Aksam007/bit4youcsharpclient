﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bit4YouClient.Lib.Models;
using Bit4YouClient.Lib.Models.Market.Request;
using Bit4YouClient.Lib.Models.Market.Response;
using Bit4YouClient.Lib.Models.Orders.Request;
using Bit4YouClient.Lib.Models.Orders.Response;
using Bit4YouClient.Lib.Models.Portfolio;
using Bit4YouClient.Lib.Models.Portfolio.Request;
using Bit4YouClient.Lib.Models.Portfolio.Response;
using Bit4YouClient.Lib.Models.Wallet.Request;
using Bit4YouClient.Lib.Models.Wallet.Response;

namespace Bit4YouClient.Lib.Services
{

    public interface IBit4YouClient
    {
        Task<OAuthResult> GetAccessTokenAsync();
        Task<UserInfo> GetUserInfoAsync();

        Task<List<WalletBalanceResponse>> WalletBalance(Simulations requestObject);
        Task<WalletTransactionResponse> WalletTransaction(WalletTransaction walletTransaction);
        Task WalletWithdrawFunds(WalletFunds walletFunds);

        Task<PortfolioListResponse> PortfolioSummary(Simulations requestObject = default);
        Task<List<PortfolioOpenOrderResponse>> PortfolioOpenOrder(Simulations requestObject = default);
        Task<List<PortfolioHistoryResponse>> PortfolioHistory(Simulations requestObject = default);
        Task<PortfolioCreateResponse> PortfolioCreateOrder(PortfolioCreateOrder portfolioCreateOrder);
        Task<PortfolioCancelResponse> PortfolioCancelOrder(PortfolioCancelOrder cancelPortfolioOrder);
        Task PortfolioCloseOrder(PortfolioClosePosition closePosition);

        Task<List<OrderListResponse>> OrderList(OrderList listRequest);
        Task<OrderInfoResponse> OrderInfo(OrderInfo orderInfo);
        Task<List<OrderPendingResponse>> OrderPending(OrderPending orderPending);
        Task<OrderCreateResponse> OrderCreate(OrderCreate createOrder);
        Task<OrderCancelResponse> OrderCancel(OrderCreate cancelOrder);

        Task<List<MarketListResponse>> MarketList();
        Task<List<MarketSummrie>> MarketSummaries();
        Task<List<MarketTicksResponse>> MarketTicks(MarketTicks marketTicks);
        Task<MarketOrderBookResponse> MarketOrderBook(MarketOrderBook marketOrderBook);
        Task<List<MarketHistoryResponse>> MarketHistory(MarketHistory marketHistory);
    }
}
