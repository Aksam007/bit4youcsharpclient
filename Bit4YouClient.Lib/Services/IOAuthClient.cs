﻿using System.Threading.Tasks;
using Bit4YouClient.Lib.Models;

namespace Bit4YouClient.Lib.Services
{
    public interface IOAuthClient
    {
        Task<OAuthResult> GetAccessTokenAsync();
    }

    
}
