﻿namespace Bit4YouClient.Lib.Models
{
    public enum ApiAction
    {
        GET,
        POST,
        PUT,
        DELETE
    }
}
