﻿namespace Bit4YouClient.Lib.Models
{
    public enum TimingForce
    {
        DAY,
        GTC,
        OPG,
        GTD,
        IOC,
        FOK
    }
}
