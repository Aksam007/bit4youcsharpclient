﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bit4YouClient.Lib.Models.Orders.Response
{
   public class OrderListResponse
    {
        public string Txid { get; set; }
        public string Type { get; set; }
        public string Market { get; set; }
        public bool IsOpen { get; set; }
        public double RequestedRate { get; set; }
        public double Quantity { get; set; }
        public double BaseQuantity { get; set; }
        public double BlockedQuantity { get; set; }
        public Remaining Remaining { get; set; }
        public Fee Fee { get; set; }
        public Position Position { get; set; }
        public double OpenTime { get; set; }
        public double UpdateTime { get; set; }
    }

   public class Remaining
   {
       public string Quantity { get; set; }
       public string Iso { get; set; }
   }

   public class Fee
   {
       public string Quantity { get; set; }
       public string Iso { get; set; }
   }

   public class Position
   {
       public int? Id { get; set; }
       public object HistoryId { get; set; }
   }
}
