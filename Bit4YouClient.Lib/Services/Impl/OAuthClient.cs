﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Bit4YouClient.Lib.Models;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Bit4YouClient.Lib.Services.Impl
{
    public class OAuthClient : IOAuthClient
    {
        private readonly ClientSettings _clientSettings;
        private readonly IHttpClientFactory _clientFactory;
        private readonly ICacheService _cacheService;


        public OAuthClient(IOptions<ClientSettings> options, IHttpClientFactory clientFactory, ICacheService cacheService)
        {
            _clientSettings = options.Value;
            _clientFactory = clientFactory;
            _cacheService = cacheService;
        }

        public async Task<OAuthResult> GetAccessTokenAsync()
        {
            var cachedToken = await _cacheService.GetAuthenticationResultFromCache(_clientSettings);
            if (cachedToken != null)
            {
                return cachedToken;
            }


            var httpClient = _clientFactory.CreateClient();
            var dataAsString = JsonConvert.SerializeObject(new OAuthRequest()
            {
                GrantType = "password",
                UserName = _clientSettings.ClientId,
                Password = _clientSettings.ClientSecret,
                Scope = string.Join(",", _clientSettings.OAuthScopes)
            });
            var content = new StringContent(dataAsString, Encoding.UTF8, "application/json");
            var result = await httpClient.PostAsync($"{_clientSettings.AccessTokenUrl}token", content);
            result.EnsureSuccessStatusCode();
            var stringContent = await result.Content.ReadAsStringAsync();
            var o = JsonConvert.DeserializeObject<JToken>(stringContent);
            if (!(o is JArray) && o["code"] != null)
            {
                throw new Exception(stringContent);
            }
            var oauthResult = o.ToObject<OAuthResult>();
            _cacheService.Add<string>(stringContent, _clientSettings.ClientSecret, oauthResult.AuthExpInSeconds);
            return oauthResult;
        }

    }
}
