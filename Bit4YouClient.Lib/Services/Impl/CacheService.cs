﻿using System;
using System.Threading.Tasks;
using Bit4YouClient.Lib.Models;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;

namespace Bit4YouClient.Lib.Services.Impl
{
    public class CacheService : ICacheService
    {
        private readonly IMemoryCache _memoryCache;
        public CacheService(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }
        public async Task<OAuthResult> GetAuthenticationResultFromCache(ClientSettings clientSettings)
        {
            return _memoryCache.TryGetValue(clientSettings.ClientSecret, out var cacheEntry) ? JsonConvert.DeserializeObject<OAuthResult>(cacheEntry as string) : null;
        }

        public void Add<T>(T o, string key, int timeInSeconds)
        {
            if (!_memoryCache.TryGetValue(key, out var cacheEntry))
            {

                cacheEntry = o;
                var cacheEntryOptions = new MemoryCacheEntryOptions()
                    .SetSlidingExpiration(TimeSpan.FromSeconds(timeInSeconds));

                _memoryCache.Set(key, cacheEntry, cacheEntryOptions);
            }
        }
    }
}
