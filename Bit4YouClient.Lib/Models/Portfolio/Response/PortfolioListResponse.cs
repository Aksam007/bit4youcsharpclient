﻿using System.Collections.Generic;

namespace Bit4YouClient.Lib.Models.Portfolio.Response
{
    public class PortfolioListResponse
    {
        public List<Item> Items { get; set; }
        public string Wallet { get; set; }
    }

    public class Item
    {
        public int Id { get; set; }
        public string BaseIso { get; set; }
        public string Market { get; set; }
        public string Invested { get; set; }
        public string Quantity { get; set; }
        public int OpenTime { get; set; }
    }
}
