﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Bit4YouClient.Lib.Models;
using Bit4YouClient.Lib.Models.Market.Request;
using Bit4YouClient.Lib.Models.Market.Response;
using Bit4YouClient.Lib.Models.Orders.Request;
using Bit4YouClient.Lib.Models.Orders.Response;
using Bit4YouClient.Lib.Models.Portfolio;
using Bit4YouClient.Lib.Models.Portfolio.Request;
using Bit4YouClient.Lib.Models.Portfolio.Response;
using Bit4YouClient.Lib.Models.Wallet;
using Bit4YouClient.Lib.Models.Wallet.Request;
using Bit4YouClient.Lib.Models.Wallet.Response;

using Microsoft.Extensions.Options;

namespace Bit4YouClient.Lib.Services.Impl
{
    public class Bit4YouClient : IBit4YouClient
    {
        private readonly IOAuthClient _oAuthClient;
        private readonly IHttpClientCustomFactory _httpClientCustomFactory;
        private readonly ClientSettings _clientSettings;

        public Bit4YouClient(IOAuthClient oAuthClient, IHttpClientCustomFactory httpClientCustomFactory, IOptions<ClientSettings> options)
        {
            _oAuthClient = oAuthClient;
            _httpClientCustomFactory = httpClientCustomFactory;
            _clientSettings = options.Value;
        }
        public Task<OAuthResult> GetAccessTokenAsync()
        {
            return _oAuthClient.GetAccessTokenAsync();
        }

        public Task<UserInfo> GetUserInfoAsync()
        {
            return _httpClientCustomFactory.PerformNetworkCall<UserInfo>(ApiAction.GET, "userinfo");
        }

        public Task<List<WalletBalanceResponse>> WalletBalance(Simulations requestObject)
        {
            return _httpClientCustomFactory.PerformNetworkCall<List<WalletBalanceResponse>>(ApiAction.POST, "wallet/balances", requestObject);
        }

        public Task<WalletTransactionResponse> WalletTransaction(WalletTransaction walletTransaction)
        {
            return _httpClientCustomFactory.PerformNetworkCall<WalletTransactionResponse>(ApiAction.POST, "wallet/transactions", walletTransaction);
        }

        public Task WalletWithdrawFunds(WalletFunds walletFunds)
        {
            return _httpClientCustomFactory.PerformNetworkCall<List<WalletBalanceResponse>>(ApiAction.POST, "wallet/send", walletFunds);
        }

        public Task<PortfolioListResponse> PortfolioSummary(Simulations requestObject = default)
        {
            return _httpClientCustomFactory.PerformNetworkCall<PortfolioListResponse>(ApiAction.POST, "portfolio/list", requestObject);
        }

        public Task<List<PortfolioOpenOrderResponse>> PortfolioOpenOrder(Simulations requestObject = default)
        {
            return _httpClientCustomFactory.PerformNetworkCall<List<PortfolioOpenOrderResponse>>(ApiAction.POST, "portfolio/open-orders", requestObject);
        }

        public Task<List<PortfolioHistoryResponse>> PortfolioHistory(Simulations requestObject = default)
        {
            return _httpClientCustomFactory.PerformNetworkCall<List<PortfolioHistoryResponse>>(ApiAction.POST, "portfolio/history", requestObject);
        }

        public Task<PortfolioCreateResponse> PortfolioCreateOrder(PortfolioCreateOrder portfolioCreateOrder)
        {
            return _httpClientCustomFactory.PerformNetworkCall<PortfolioCreateResponse>(ApiAction.POST, "portfolio/create-order", portfolioCreateOrder);
        }

        public Task<PortfolioCancelResponse> PortfolioCancelOrder(PortfolioCancelOrder cancelPortfolioOrder)
        {
            return _httpClientCustomFactory.PerformNetworkCall<PortfolioCancelResponse>(ApiAction.POST, "portfolio/cancel-order", cancelPortfolioOrder);
        }

        public Task PortfolioCloseOrder(PortfolioClosePosition closePosition)
        {
            return _httpClientCustomFactory.PerformNetworkCall<PortfolioCancelResponse>(ApiAction.POST, "portfolio/close", closePosition);
        }

        public Task<List<OrderListResponse>> OrderList(OrderList listRequest)
        {
            return _httpClientCustomFactory.PerformNetworkCall<List<OrderListResponse>>(ApiAction.POST, "order/list", listRequest);
        }

        public Task<OrderInfoResponse> OrderInfo(OrderInfo orderInfo)
        {
            return _httpClientCustomFactory.PerformNetworkCall<OrderInfoResponse>(ApiAction.POST, "order/info", orderInfo);
        }

        public Task<List<OrderPendingResponse>> OrderPending(OrderPending orderPending)
        {
            return _httpClientCustomFactory.PerformNetworkCall<List<OrderPendingResponse>>(ApiAction.POST, "order/pending", orderPending);
        }

        public Task<OrderCreateResponse> OrderCreate(OrderCreate createOrder)
        {
            return _httpClientCustomFactory.PerformNetworkCall<OrderCreateResponse>(ApiAction.POST, "order/create", createOrder);
        }

        public Task<OrderCancelResponse> OrderCancel(OrderCreate cancelOrder)
        {
            return _httpClientCustomFactory.PerformNetworkCall<OrderCancelResponse>(ApiAction.POST, "order/cancel", cancelOrder);
        }

        public Task<List<MarketListResponse>> MarketList()
        {
            return _httpClientCustomFactory.PerformNetworkCall<List<MarketListResponse>>(ApiAction.GET, "market/list");
        }

        public Task<List<MarketSummrie>> MarketSummaries()
        {
            return _httpClientCustomFactory.PerformNetworkCall<List<MarketSummrie>>(ApiAction.GET, "market/summaries");
        }

        public Task<List<MarketTicksResponse>> MarketTicks(MarketTicks marketTicks)
        {
            return _httpClientCustomFactory.PerformNetworkCall<List<MarketTicksResponse>>(ApiAction.POST, "market/ticks", marketTicks);
        }

        public Task<MarketOrderBookResponse> MarketOrderBook(MarketOrderBook marketOrderBook)
        {
            return _httpClientCustomFactory.PerformNetworkCall<MarketOrderBookResponse>(ApiAction.POST, "market/orderbook", marketOrderBook);
        }

        public Task<List<MarketHistoryResponse>> MarketHistory(MarketHistory marketHistory)
        {
            return _httpClientCustomFactory.PerformNetworkCall<List<MarketHistoryResponse>>(ApiAction.POST, "market/history", marketHistory);
        }
    }
}
