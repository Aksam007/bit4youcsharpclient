﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

using Bit4YouClient.Lib.Models;

using Microsoft.Extensions.Options;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace Bit4YouClient.Lib.Services.Impl
{
    public class HttpClientCustomFactory : IHttpClientCustomFactory
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly IOAuthClient _oAuthClient;
        private readonly ClientSettings _clientSettings;

        public HttpClientCustomFactory(IOptions<ClientSettings> options, IHttpClientFactory clientFactory, IOAuthClient oAuthClient)
        {
            _clientFactory = clientFactory;
            _oAuthClient = oAuthClient;
            _clientSettings = options.Value;
        }
        public async Task<T> PerformNetworkCall<T>(ApiAction apiAction, string resourceUri, object requestObject = default, Dictionary<string, string> queryParams = default)
        {
            var requestMessage = new HttpRequestMessage();
            var client = _clientFactory.CreateClient();
            var cachedResult = await _oAuthClient.GetAccessTokenAsync();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", cachedResult.AccessToken);
            switch (apiAction)
            {
                case ApiAction.GET:
                    {
                        requestMessage.Method = HttpMethod.Get;
                        requestMessage.RequestUri = new Uri($"{_clientSettings.ApiUrl}{resourceUri}");
                        if (resourceUri.Equals("userinfo"))
                        {
                            requestMessage.RequestUri = new Uri($"{_clientSettings.AccessTokenUrl}{resourceUri}");
                        }
                        var result = await client.SendAsync(requestMessage);
                        return await GetDeserializedResponse<T>(result);
                    }
                case ApiAction.POST:
                    {
                        return await PostDataToApi<T>(requestMessage, client, resourceUri, requestObject);
                    }
                case ApiAction.PUT:
                    break;
                case ApiAction.DELETE:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(apiAction), apiAction, null);
            }

            return default(T);
        }


        private async Task<T> GetDeserializedResponse<T>(HttpResponseMessage result)
        {
            var stringContent = await result.Content.ReadAsStringAsync();
            var o = JsonConvert.DeserializeObject<JToken>(stringContent);
            if (!(o is JArray) && (o["code"] != null || o["error"] != null))
            {
                throw new Exception(stringContent);
            }
            result.EnsureSuccessStatusCode();
            return o.ToObject<T>();
        }

        private async Task<T> PostDataToApi<T>(HttpRequestMessage requestMessage, HttpClient client, string resourceUri, object requestObject)
        {
            requestMessage.Method = HttpMethod.Post;
            requestMessage.RequestUri = new Uri($"{_clientSettings.ApiUrl}{resourceUri}");
            var serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
            var json = JsonConvert.SerializeObject(requestObject, serializerSettings);
            requestMessage.Content = new StringContent(json, Encoding.UTF8, "application/json");
            var result = await client.SendAsync(requestMessage);
            return await GetDeserializedResponse<T>(result);
        }
    }
}
