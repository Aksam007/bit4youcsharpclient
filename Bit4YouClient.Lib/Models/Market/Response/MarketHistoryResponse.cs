﻿namespace Bit4YouClient.Lib.Models.Market.Response
{
    public class MarketHistoryResponse
    {
        public bool Buy { get; set; }
        public string Id { get; set; }
        public string Quantity { get; set; }
        public string Rate { get; set; }
        public int Time { get; set; }
    }
}
