﻿using System;
using System.Collections.Generic;
using System.Text;

using Newtonsoft.Json;

namespace Bit4YouClient.Lib.Models.Portfolio.Response
{
    public class PortfolioOpenOrderResponse
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("execute_at")]
        public string ExecuteAt { get; set; }

        [JsonProperty("remaining_quantity")]
        public string RemainingQuantity { get; set; }

        [JsonProperty("remaining_iso")]
        public string RemainingIso { get; set; }

        [JsonProperty("isOpening")]
        public int IsOpening { get; set; }

        [JsonProperty("market")]
        public string Market { get; set; }

        [JsonProperty("baseCurrency")]
        public string BaseCurrency { get; set; }

        [JsonProperty("invested")]
        public string Invested { get; set; }

        [JsonProperty("quantity")]
        public string Quantity { get; set; }

        [JsonProperty("open_time")]
        public int OpenTime { get; set; }

        [JsonProperty("close_time")]
        public object CloseTime { get; set; }
    }
}
