﻿using Newtonsoft.Json;

namespace Bit4YouClient.Lib.Models.Portfolio.Response
{
    public class PortfolioCreateResponse
    {
        [JsonProperty("txid")]
        public string Txid { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("market")]
        public string Market { get; set; }

        [JsonProperty("isOpen")]
        public bool IsOpen { get; set; }

        [JsonProperty("requested_rate")]
        public string RequestedRate { get; set; }

        [JsonProperty("quantity")]
        public string Quantity { get; set; }

        [JsonProperty("base_quantity")]
        public string BaseQuantity { get; set; }

        [JsonProperty("blocked_quantity")]
        public string BlockedQuantity { get; set; }

        [JsonProperty("remaining")]
        public Remaining Remaining { get; set; }

        [JsonProperty("fee")]
        public Fee Fee { get; set; }

        [JsonProperty("position")]
        public Position Position { get; set; }

        [JsonProperty("open_time")]
        public int OpenTime { get; set; }

        [JsonProperty("update_time")]
        public int UpdateTime { get; set; }
    }

    public class Remaining
    {
        [JsonProperty("quantity")]
        public string Quantity { get; set; }

        [JsonProperty("iso")]
        public string Iso { get; set; }
    }

    public class Fee
    {
        [JsonProperty("quantity")]
        public string Quantity { get; set; }

        [JsonProperty("iso")]
        public string Iso { get; set; }
    }

    public class Position
    {
        [JsonProperty("id")]
        public object Id { get; set; }

        [JsonProperty("history_id")]
        public object HistoryId { get; set; }
    }
}
