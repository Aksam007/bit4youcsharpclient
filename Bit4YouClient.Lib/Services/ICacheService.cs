﻿using System.Threading.Tasks;
using Bit4YouClient.Lib.Models;

namespace Bit4YouClient.Lib.Services
{
    public interface ICacheService
    {
        Task<OAuthResult> GetAuthenticationResultFromCache(ClientSettings clientSettings);
        void Add<T>(T o, string key, int timeInSeconds);
    }
}
