﻿namespace Bit4YouClient.Lib.Models.Market.Request
{
    public class MarketTicks
    {
        public string Market { get; set; }
        public int Interval { get; set; }
    }
}
