﻿namespace Bit4YouClient.Lib.Models.Market.Response
{
    public class MarketSummrie
    {
        public string Market { get; set; }
        public double MarketCap { get; set; }
        public double High { get; set; }
        public double Low { get; set; }
        public double Volume { get; set; }
        public double Last { get; set; }
        public double PrevDay { get; set; }
        public double Bid { get; set; }
        public double Ask { get; set; }
        public double Open { get; set; }
    }
}
