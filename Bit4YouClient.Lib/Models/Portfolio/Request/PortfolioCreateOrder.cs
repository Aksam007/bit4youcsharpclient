﻿namespace Bit4YouClient.Lib.Models.Portfolio.Request
{
    public class PortfolioCreateOrder : Simulations
    {
        public string Market { get; set; }
        public double Quantity { get; set; }
        public double Rate { get; set; }
    }
}
