﻿using Newtonsoft.Json;

namespace Bit4YouClient.Lib.Models.Wallet.Response
{
    public class WalletBalanceResponse
    {
        [JsonProperty("iso")]
        public string Iso { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("tx")]
        public string Tx { get; set; }

        [JsonProperty("tx_enabled")]
        public bool TxEnabled { get; set; }

        [JsonProperty("erc20")]
        public bool Erc20 { get; set; }

        [JsonProperty("balance")]
        public string Balance { get; set; }

        [JsonProperty("tx_explorer")]
        public string TxExplorer { get; set; }

        [JsonProperty("digits")]
        public int? Digits { get; set; }
    }
}
