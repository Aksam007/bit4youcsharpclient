﻿using System.Collections.Generic;

namespace Bit4YouClient.Lib.Models.Wallet.Response
{
    public class WalletTransactionResponse
    {
        public double Balance { get; set; }
        public double Pages { get; set; }
        public List<Tx> Txs { get; set; }
    }

    public class Meta
    {
        public bool Pending { get; set; }
        public object PaidValue { get; set; }
        public string PaidAsset { get; set; }
        public string FeeAsset { get; set; }
        public string Market { get; set; }
    }

    public class Tx
    {
        public string Txid { get; set; }
        public object Block { get; set; }
        public object Confirmations { get; set; }
        public string Fee { get; set; }
        public int Time { get; set; }
        public string Quantity { get; set; }
        public string Type { get; set; }
        public Meta Meta { get; set; }
    }
}
