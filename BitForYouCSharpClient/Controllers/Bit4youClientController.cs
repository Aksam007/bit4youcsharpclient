﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bit4YouClient.Lib.Models;
using Bit4YouClient.Lib.Models.Market.Request;
using Bit4YouClient.Lib.Models.Market.Response;
using Bit4YouClient.Lib.Models.Orders.Request;
using Bit4YouClient.Lib.Models.Orders.Response;
using Bit4YouClient.Lib.Models.Portfolio;
using Bit4YouClient.Lib.Models.Portfolio.Request;
using Bit4YouClient.Lib.Models.Portfolio.Response;
using Bit4YouClient.Lib.Models.Wallet;
using Bit4YouClient.Lib.Models.Wallet.Request;
using Bit4YouClient.Lib.Models.Wallet.Response;
using Bit4YouClient.Lib.Services;

namespace BitForYouCSharpClient.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class Bit4YouClientController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<Bit4YouClientController> _logger;
        private readonly IBit4YouClient _bit4YouClient;
        private readonly IHttpClientCustomFactory _httpClientCustomFactory;

        public Bit4YouClientController(ILogger<Bit4YouClientController> logger, IBit4YouClient bit4YouClient, IHttpClientCustomFactory httpClientCustomFactory)
        {
            _logger = logger;
            _bit4YouClient = bit4YouClient;
            _httpClientCustomFactory = httpClientCustomFactory;
        }


        #region Markeet
        [HttpGet("markeetList")]
        public async Task<IEnumerable<MarketListResponse>> markeetList()
        {
            return await _bit4YouClient.MarketList();
        }

        [HttpGet("markeetSummaries")]
        public async Task<IEnumerable<MarketSummrie>> markeetSummaries()
        {
            return await _bit4YouClient.MarketSummaries();
        }

        [HttpGet("markeetTics")]
        public async Task<IEnumerable<MarketTicksResponse>> markeetTics()
        {
            return (await _bit4YouClient.MarketTicks(new MarketTicks()
            {
                Interval = 60,
                Market = "USDT-BTC"
            })).Take(10);
        }

        [HttpGet("markeetOrderBooks")]
        public async Task<MarketOrderBookResponse> markeetOrderBooks()
        {
            return await _bit4YouClient.MarketOrderBook(new MarketOrderBook()
            {
                Market = "USDT-BTC",
                Limit = 50,
                State = false
            });
        }

        [HttpGet("markeetHistory")]
        public async Task<List<MarketHistoryResponse>> markeetHistory()
        {
            return await _bit4YouClient.MarketHistory(new MarketHistory()
            {
                From = "String",
                Limit = 50,
                Market = "USDT-BTC",
                To = "String"
            });
        }


        #endregion

        #region User
        [HttpGet("User")]
        public async Task<UserInfo> User()
        {
            return await _bit4YouClient.GetUserInfoAsync();
        }


        #endregion

        #region Wallet
        [HttpGet("WalletBalance")]
        public async Task<List<WalletBalanceResponse>> WalletBalance()
        {
            return await _bit4YouClient.WalletBalance(new Simulations()
            {
                Simulation = true
            });
        }

        [HttpGet("WalletTransactions")]
        public async Task<WalletTransactionResponse> WalletTransactions()
        {
            return await _bit4YouClient.WalletTransaction(new WalletTransaction()
            {
                Simulation = true,
                Iso = "BTC"
            });
        }

        [HttpGet("WalletSend")]
        public async Task WalletSend()
        {
            await _bit4YouClient.WalletWithdrawFunds(new WalletFunds()
            {
                Iso = "BTC",
                Address = "1CK6KHY6MHgYvmRQ4PAafKYDrg1eaaaaaa",
                Quantity = 1.05
            });
        }


        #endregion

        #region Order
        [HttpGet("OrderList")]
        public async Task<List<OrderListResponse>> OrderList()
        {
            return await _bit4YouClient.OrderList(new OrderList()
            {
                Simulation = true,
                Market = "USDT-BTC",
                Limit = 10,
                Page = 0
            });
        }

        [HttpGet("OrderInfo")]
        public async Task<OrderInfoResponse> OrderInfo()
        {
            
           return  await _bit4YouClient.OrderInfo(new OrderInfo()
            {
                Simulation = true,
                Txid = "db78faa89f08062bfebeacb51365fadb08b63da6"
             });
        }

        [HttpGet("OrderPending")]
        public async Task<List<OrderPendingResponse>> OrderPending()
        {

           return await _bit4YouClient.OrderPending(new OrderPending()
            {
                Simulation = true
            });
        }

        [HttpGet("OrderCreate")]
        public async Task<OrderCreateResponse> OrderCreate()
        {

           return await _bit4YouClient.OrderCreate(new OrderCreate()
            {
                Simulation = true,
                Market = "USDT-BTC",
                Quantity = 10,
                QuantityIso = "BTC",
                Rate = 1.5,
                Type = "buy",
                //ClientId ="",
                //TimingForce = TimingForce.DAY
            });
        }

        [HttpGet("CencelOrder")]
        public async Task<OrderCancelResponse> CencelOrder()
        {

           return await _bit4YouClient.OrderCancel(new OrderCreate()
            {
                Simulation = true,
                Txid = "30eb76fa30154f48883446e274e7a0bd"
           });
        }


        #endregion

        #region Portfolio
        [HttpGet("PortfolioSummary")]
        public async Task<PortfolioListResponse> PortfolioSummary()
        {
           return  await _bit4YouClient.PortfolioSummary(new Simulations()
            {
                Simulation = true
            });
        }
        [HttpGet("PortfolioOpenOrder")]
        public async Task<List<PortfolioOpenOrderResponse>> PortfolioOpenOrder()
        {
            return await _bit4YouClient.PortfolioOpenOrder(new Simulations()
            {
                Simulation = true
            });
        }

        [HttpGet("PortfolioHistory")]
        public async Task<List<PortfolioHistoryResponse>> PortfolioHistory()
        {
            return await _bit4YouClient.PortfolioHistory(new Simulations()
            {
                Simulation = true
            });
        }

        [HttpGet("PortfolioCreateOrder")]
        public async Task<PortfolioCreateResponse> PortfolioCreateOrder()
        {
            return await _bit4YouClient.PortfolioCreateOrder(new PortfolioCreateOrder()
            {
                Simulation = true,
                Market = "USDT-BTC",
                Quantity = 0.55,
                Rate = 3555.36
            });
        }

        [HttpGet("PortfolioCancelOrder")]
        public async Task<PortfolioCancelResponse> PortfolioCancelOrder()
        {
            return await _bit4YouClient.PortfolioCancelOrder(new PortfolioCancelOrder()
            {
                Simulation = true,
                Id = 177170
            });
        }

        [HttpGet("PortfolioCloseOrder")]
        public async Task PortfolioCloseOrder()
        {
             await _bit4YouClient.PortfolioCloseOrder(new PortfolioClosePosition()
            {
                Simulation = false,
                Id = 178015
             });
        }

        #endregion

    }
}
