﻿namespace Bit4YouClient.Lib
{
    public class ClientSettings
    {
        public string AccessTokenUrl { get; set; }
        public string ApiUrl { get; set; }
        public string UserInfo { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string[] OAuthScopes { get; set; }
    }
}
