﻿namespace Bit4YouClient.Lib.Models.Portfolio.Response
{
    public class PortfolioCancelResponse
    {
        public string Status { get; set; }
        public string Message { get; set; }
    }
}
