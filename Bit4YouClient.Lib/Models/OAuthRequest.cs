﻿using Newtonsoft.Json;

namespace Bit4YouClient.Lib.Models
{
    public class OAuthRequest
    {
        [JsonProperty("grant_type")]
        public string GrantType { get; set; }
        [JsonProperty("scope")]
        public string Scope { get; set; }
        [JsonProperty("username")]
        public string UserName { get; set; }
        [JsonProperty("password")]
        public string Password { get; set; }
    }
}
