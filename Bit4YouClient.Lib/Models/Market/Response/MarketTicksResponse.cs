﻿namespace Bit4YouClient.Lib.Models.Market.Response
{
    public class MarketTicksResponse
    {
        public int Time { get; set; }
        public string Open { get; set; }
        public string Close { get; set; }
        public string Low { get; set; }
        public string High { get; set; }
        public string Volume { get; set; }
        public string MarketVolume { get; set; }
    }
}
