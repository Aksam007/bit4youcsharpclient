﻿namespace Bit4YouClient.Lib.Models.Orders.Request
{
    public class OrderList:Simulations
    {
        public int Page { get; set; }
        public int Limit { get; set; }
        public string Market { get; set; }
    }
}
