﻿namespace Bit4YouClient.Lib.Models.Market.Request
{
    public class MarketOrderBook
    {
        public string Market { get; set; }
        public int Limit { get; set; }
        public bool State { get; set; }
    }
}
