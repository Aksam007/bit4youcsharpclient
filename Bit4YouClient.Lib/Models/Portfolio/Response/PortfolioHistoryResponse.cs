﻿using Newtonsoft.Json;

namespace Bit4YouClient.Lib.Models.Portfolio.Response
{
   public class PortfolioHistoryResponse
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("market")]
        public string Market { get; set; }

        [JsonProperty("baseCurrency")]
        public string BaseCurrency { get; set; }

        [JsonProperty("invested")]
        public string Invested { get; set; }

        [JsonProperty("closed_amount")]
        public string ClosedAmount { get; set; }

        [JsonProperty("quantity")]
        public string Quantity { get; set; }

        [JsonProperty("fee")]
        public string Fee { get; set; }

        [JsonProperty("open_time")]
        public int OpenTime { get; set; }

        [JsonProperty("close_time")]
        public int CloseTime { get; set; }
    }
}
