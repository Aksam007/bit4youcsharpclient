﻿namespace Bit4YouClient.Lib.Models.Market.Request
{
    public class MarketHistory
    {
        public string Market { get; set; }
        public int Limit { get; set; }
        public string From { get; set; }
        public string To { get; set; }
    }
}
