﻿using System.Collections.Generic;

namespace Bit4YouClient.Lib.Models.Market.Request
{
    public class MarketListResponse
    {
        public string Iso { get; set; }
        public string Name { get; set; }
        public int Precision { get; set; }
        public double Value { get; set; }
        public double Change { get; set; }
        public double Spread { get; set; }
        public string Category { get; set; }
    }

    public class MarketList
    {
        public List<MarketListResponse> MarketLisResponse { get; set; }
    }
}
